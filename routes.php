<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
/* require_once 'controllers/LoginController.php'; */
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/MeetupController.php';

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/meetup';

SimpleRouter::group(['prefix' => $prefix], function () {
 /*    SimpleRouter::match(['get', 'post'], '/login', 'loginController@login')->name('login'); */
    SimpleRouter::get('/', 'Defaultcontroller@getMeetup'); 
    SimpleRouter::get('/{id}','DefaultController@getMeetupById');
    SimpleRouter::delete('/events/{id}','MeetupController@deleteMeetup');
    SimpleRouter::put('/events/{id}','MeetupController@updateMeetup');
    SimpleRouter::post('/events/','MeetupController@createMeetup');
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll');/* ->addMiddleware(APIMiddleware::class) */
    SimpleRouter::get('/subscribers/{id}', 'SubscriberController@getSubscriberById');/* ->addMiddleware(APIMiddleware::class) */

});