<?php

class LocationModel{
    private $address;
    private $city;
    private $zip_code;
    private $description;

    //getters
    public function getAddress(){
        return $this->address;
    }
    public function getCity(){
        return $this->city;
        
    }
    public function getZipCode(){
        return $this->zip_code;
        
    }
    public function getDescription(){
        return $this->description;
    }
    //setters
    public function setAdress($value){
        $this->adress=$value;
    }
    public function setCity($value){
        $this->city = $value;
    }
    public function setZipCode($value){
        $this->zip_code=$value;
    }
    public function setDescription($value){
        $this->description = $value;
    }

}
?>