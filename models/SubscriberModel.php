<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:43
 */
include_once('DB.php');
class Subscriber {
    private $firstName;
    private $lastName;
    private $mailAddr;
    private $dateSubscription;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getMailAddr() {
        return $this->mailAddr;
    }

    public function setMailAddr($mailAddr) {
        $this->mailAddr = $mailAddr;
    }

    public function getDateSubscription() {
        return $this->dateSubscription;
    }

    public function setDateSubscription($dateSubscription) {
        $this->dateSubscription = $dateSubscription;
    }

    public function getAll() {
        // Ici votre requête SQL (peut-être qu'un extends serait sympa pour la connexion en DB...)
        // Les données ci-dessous son fakes !
        $bdd=DB::connexion();
        $req = $bdd->query('SELECT * FROM `subscriber`');
       $result= $req->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public function getById($id){
        $bdd=DB::connexion();
        $req = $bdd->prepare('SELECT * FROM `subscriber` WHERE id = :id');
        $req->execute(array("id"=>$id));
        $result = $req->fetchAll(PDO::FETCH_CLASS,"Subscriber");
        return $result;
    }
    
    public function addSubcriber($fn,$ln,$mail){
        $bdd=DB::connexion();
        $req=$bdd->prepare('INSERT INTO `Subscriber` (`first_name`,`last_name`,`mail_addr`) VALUES (:first_name,:last_name,:mail_addr)');
        $req->execute(array("first_name"=>$fn,"last_name"=>$ln,"mail_addr"=>$mail));
    }
    
    public function updateSubcriber($fn,$ln,$mail){
        $bdd=DB::connexion();
        $req=$bdd->prepare('UPDATE `meetup` SET `first_name`=:first_name,`last_name`=:last_name,`mail_addr`=:mail_addr WHERE id = :id');
        $req->execute(array("first_name"=>$fn,"last_name"=>$ln,"mail_addr"=>$mail));
    }
    
    public function delete($id){
        $bdd=connexion();
        $req=$bdd->prepare('DELETE FROM `Subscriber` WHERE `id`=:id');
        $req->execute(array("id"=>$id));
    }
}
