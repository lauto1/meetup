<?php
class SpeakerModel{
    private $first_name;
    private $last_name;
    private $description;

    //getters

    public function getFirst_name(){
        return $this->first_name;
    }
    public function getLast_Name(){
        return $this->last_name;
    }
    public function getDescription(){
        return $this->description;
    }
    //setters

    public function setFirst_name($value){
        $this->first_name= $value;
    }
    public function setLast_name($value){
        $this->last_name=$value;
    }
    public function setDescription($value){
        $this->description = $value;
    }
    public function getById($id){
        include('DB.php');
        $bdd = Db::connexion();
        $req=$bdd->prepare('SELECT * FROM `speaker` WHERE id= :id');
        $req->execute(array("id"=>$id));
        return $req;
    }
    public function getAll(){
        include('DB.php');
        $bdd = Db::connexion();
        $req = $bdd->prepare('SELECT * FROM `speaker`');
        $req->execute();
        return $req;
    }

    public function update($id,$fname,$lname,$description){
        include('DB.php');
        $bdd =Db::connexion();
        $req=$bdd->prepare('UPDATE `speaker` SET id = :id , first_name = :first_name ,last_name= :last_name, description= :description');//a finir
        $req->execute(array("id"=>$id,"first_name"=>$fname,"last_name"=>$lname,"description"=>$description));
        return $req;
    }

    public function delete($id){
        include('DB.php');
        $bdd=Db::connexion();
        $req = $bdd->prepare('DELETE FROM `speaker` WHERE id = :id');
        $req->execute(array("id" => $id));
        return $req;
    }
}
?>