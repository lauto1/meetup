<?php
include_once('DB.php');
class Meetup{
    private $titre;
    private $description;
    private $date;
    private $location_id;
    private $subscriber_fk;
    private $speaker_fk;

    //getters
    public function getTitre(){
        return $this->titre;
    }
    public function getDescription(){
        return $this->descritption;
    }
    public function getDate(){
        return $this->date;
    }
    public function getLocation_id(){
        return $this->location_id;
    }
    public function getNb_subscriber(){
        $this->nb_subscriber;
    }
    public function getNb_speaker(){
        $this->nb_speaker;
    }
    //setters
     public function setTitre($value){
         $this->titre = $value;
     }
     public function setLocation($value){
        $this->location_id = $value;
    }
    public function setDescription($value){
        $this->description = $value;
    }
    public function setDate($value){
        $this->date = $value;
    }
    public function setNb_subscriber($value){
        $this->nb_subscriber = $value;
    }
    public function setNb_speaker($value){
        $this->nb_speaker = $value;
    }
    //requests
    public function getById($id){

        $bdd=Db::connexion();
        $req=$bdd->prepare('SELECT * FROM `meetup` WHERE id = :id');
        $req->execute(array("id"=>$id));
        $result = $req->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function getAll(){

        $bdd = Db::connexion();
        $req = $bdd->query('SELECT * FROM `meetup`');
        $result = $req->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function update($id,$titre,$description ,$date , $location_id){

        $bdd =Db::connexion();
        $req=$bdd->prepare('UPDATE `meetup` SET titre= :titre , description= :description , date= :date , location_id = :location_id WHERE id = :id');
        $req->execute(array("id"=>$id,"titre"=>$titre , "description"=>$description, "date"=>$date,"location_id"=>$location_id));
    }

    public function delete($id){

        $bdd=Db::connexion();
        $req = $bdd->prepare('DELETE FROM `meetup` WHERE id = :id');
        $req->execute(array("id"=>$id));
    }
    public function nb_subscriber($id){

        $bdd=Db::connexion();
        $req=$bdd->prepare('SELECT COUNT FROM `meetup`');
    }
    public function create($titre,$description,$date,$location_id){
      
            $bdd=Db::connexion();
            $req= $bdd->prepare("INSERT INTO `meetup`(`titre`, `description`, `date`, `location_Id`) VALUES (:titre,:description,:date,:location)");
            $req->execute(array(
                "titre"=>$titre,
                "description"=>$description,
                "date"=>$date,
                "location"=>$location_id
            ));
    }

}  
?>