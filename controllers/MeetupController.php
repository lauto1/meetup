
   <?php
    require_once 'controllers/Controller.php';
    require_once 'models/MeetupModel.php';

    Class MeetupController extends Controller{
        private $id;
        private $description;
        private $date;
        private $location;
        private $titre;
        
        public function updateMeetup($id){
            parse_str(file_get_contents("php://input"),$PUT);
            var_dump($PUT);
            $titre= $PUT['titre'];
            $description = $PUT['desc'];
            $date= $PUT['date'];
            $location_id= $PUT['location'];
            $meetup = new Meetup();
            $meetup->update($id,$titre,$description ,$date , $location_id);
            return json_encode($PUT);
        }

        public function deleteMeetup($id){
            /*parse_str(file_get_contents("php://input"),$DEL);*/
            $meetup = new Meetup();
            return json_encode($meetup->delete($id));
        }

        public function createMeetup(){
            parse_str(file_get_contents("php://input"),$POST);
            var_dump($POST);
            $titre = $POST['titre'];
            $date = $POST['date'];
            $location_id = $POST['lieu'];
            $description = $POST['desc'];
            $meetup = new Meetup();
            $meetup->create($titre,$description,$date,$location_id);
            return json_encode($POST);
        }
    }
?>