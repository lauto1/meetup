<?php

require_once 'controllers/Controller.php';
require_once 'models/SubscriberModel.php';

class SubscriberController extends Controller {
    function getAll() {
        $subscriber = new Subscriber();
        echo json_encode($subscriber->getAll());
    }
    public function getSubscriberByID($id){
        $subscriber = new Subscriber();
        echo json_encode($subscriber->getById($id));
    }
    public function createSubscriber(){
        $first_name = $_POST[''];
        $last_name = $_POST[''];
        $mail = $_POST[''];
        $subscriber = new Subscriber();
        return json_encode($subscriber->createSubscriber($first_name,$last_name,$mail));
    }
    public function updateSubscriber($id){
        parse_str(file_get_contents("php://input"),$PUT);
        var_dump($PUT);
        $first_name = $PUT[''];
        $last_name = $PUT[''];
        $mail=$PUT[''];
        $subscriber = new Subscriber();
        return json_encode($subscriber->updateSubscriber($id,$first_name,$last_name,$mail));
    }
    public function deleteSubscriber($id){
        $subscriber = new Subscriber();
        return json_encode($subscriber->delete($id));
    }
}