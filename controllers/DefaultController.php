<?php

require_once 'controllers/Controller.php';
require_once 'models/MeetupModel.php';

class DefaultController extends Controller {
    public function defaultAction() {
        $data = array('Message' => 'Welcome on my first API');
        echo json_encode($data);
    }

    public function getMeetup(){
        $meetup = new Meetup();
        echo json_encode($meetup->getAll());
    }
    public function getMeetupById($id){
        $meetup = new Meetup();
        return json_encode($meetup->getById($id));
    }
}