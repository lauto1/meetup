$(document).ready(function () {
    //afficher un meetup
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");


    $('form').on('submit',function(event){
            event.preventDefault();
            form = new FormData(this);
            var titre = form.get('titre');
            var date = form.get('date');
            var lieu = form.get('lieu');
            var desc = form.get('desc');
            
            $.ajax({
                url:"/meetup/events/"+id,
                method:"PUT",
                data: {titre,date,lieu,desc},
                dataType : 'json',
                encode : 'true',
                success: function(){
                    window.location.href = window.location.href ;
                }
            });
        });
    //update meetup


    //delete meetup
});