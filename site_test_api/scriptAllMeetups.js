var url = "/meetup/site_test_api";
$(document).ready(function(){
    //Afficher tous les meetups
    function displayMeetups(){
        $.ajax({
            url:'/meetup/',
            method : 'GET'
        })
            .done(function(data){
            json = JSON.parse(data);

            json.forEach(function(element){
              $('#display').append("<div class='meetup card'><div class='btns'><div><a href='"+url+"/meetup.html?id="+element.id+"'><button class='btn btn-primary'>Plus d'infos</button></a><a href='"+url+"/edit.html?id="+element.id+"'><button class='btn btn-info'>Edit</button></a></div></div><h3 class='title card-title m-auto'>"+element.titre+"</h3><h4 class='date'>"+element.date+"</h4><div id='content' class='card-block'><p>"+element.description+"</p></div><form class='del'><input type='submit' value='delete' class='btn btn-danger'><input type='hidden' name='id' 'value='"+element.id+"'></form></div>");
            });
        });
    }
    displayMeetups();
 //redirections 
 
// ('body').on("click",'.title',function(){
//  window.location.replace(url+'/meetup.html?id='+)
//   $(location).attr("href", url+'/meetup');
// });
    
//delete meetup
    $('body').on("submit",".del",function(event){
    
     event.preventDefault();
     form = new FormData(this);
     var id = form.get('id');
        $.ajax({
         url:"/meetup/events/"+id,
         method:"DELETE",
        success: function(){
            window.location.reload(true);
        }
        })
    });
    //createMeetup

        $('form').on('submit',function(event){
            event.preventDefault();
            form = new FormData(this);
            var titre = form.get('titre');
            var date = form.get('date');
            var lieu = form.get('lieu');
            var desc = form.get('desc');
            
            $.ajax({
                url:"/meetup/events/",
                method:"POST",
                data: {titre,date,lieu,desc},
                dataType : 'json',
                encode : 'true',
                success: function(){
                    window.location.href = window.location.href ;
                }
            });
        });
});

/*function delete_event(event){
    event.preventDefault();
    console.log('test');
}*/